export default {
  // Endpoints
  FORTIFOX_BASE_API: 'http://localhost:3000',
  endpoints: {
    auth: {
      login: '/auth/login',
      register: '/auth/register',
      refresh: '/auth/refresh-token',
      logout: '/auth/logout',
    },
  },
  tokenType: 'Bearer',
  storageTokenKeyName: 'accessToken',
  storageRefreshTokenKeyName: 'refreshToken',
};
