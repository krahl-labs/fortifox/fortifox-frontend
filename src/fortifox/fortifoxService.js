import appConfig from './config';

export default class FortiFoxService {
  appConfig = { ...appConfig };

  axiosIns = null;

  subscribers = [];

  constructor(axiosIns) {
    this.axiosIns = axiosIns;

    this.axiosIns.interceptors.request.use(
      config => {
        const accessToken = this.getToken();

        if (accessToken) {
          // eslint-disable-next-line no-param-reassign
          config.headers.Authorization = `${this.appConfig.tokenType} ${accessToken}`;
        }
        return config;
      },
      error => Promise.reject(error),
    );

    this.axiosIns.interceptors.response.use(
      response => response,
      error => {
        const { response } = error;

        if (response && response.status === 401) {
          console.log(response)
        }
        return Promise.reject(error);
      },
    );
  }

  getToken() {
    return localStorage.getItem(this.appConfig.storageTokenKeyName);
  }

  login(...args) {
    return this.axiosIns.post(`${this.appConfig.FORTIFOX_BASE_API}${this.appConfig.endpoints.auth.login}`, ...args);
  }

  setToken(value) {
    localStorage.setItem(this.appConfig.storageTokenKeyName, value)
  }

  register(...args) {
    return this.axiosIns.post(`${this.appConfig.FORTIFOX_BASE_API}${this.appConfig.endpoints.auth.register}`, ...args);
  }

  get(endpoint, ...args) {
    return this.axiosIns.get(`${this.appConfig.FORTIFOX_BASE_API}${endpoint}`, ...args);
  }

  checkToken() {
    return this.axiosIns.get(`${this.appConfig.FORTIFOX_BASE_API}/check_token`);
  }
}
