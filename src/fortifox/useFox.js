import axios from '@axios'
import FortiFoxService from './fortifoxService'

export default new FortiFoxService(axios)
