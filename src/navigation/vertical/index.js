export default [
  {
    title: 'Home',
    route: 'home',
    icon: 'HomeIcon',
  },
  {
    title: 'Users',
    route: 'users',
    icon: 'UsersIcon',
  },
]
