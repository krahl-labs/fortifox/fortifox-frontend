export default {
  // Endpoints
  loginEndpoint: 'http://localhost:3000/auth/login',
  registerEndpoint: 'http://localhost:3000/auth/register',
  refreshEndpoint: 'http://localhost:3000/auth/refresh-token',
  logoutEndpoint: 'http://localhost:3000/auth/logout',

  // This will be prefixed in authorization header with token
  // e.g. Authorization: Bearer <token>
  tokenType: 'Bearer',

  // Value of this property will be used as key to store JWT token in storage
  storageTokenKeyName: 'accessToken',
  storageRefreshTokenKeyName: 'refreshToken',
}
